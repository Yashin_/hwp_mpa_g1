package project.lib;

import ev3dev.sensors.ev3.EV3ColorSensor;
import ev3dev.sensors.ev3.EV3GyroSensor;
import ev3dev.sensors.ev3.EV3TouchSensor;
import ev3dev.sensors.ev3.EV3UltrasonicSensor;

import lejos.hardware.port.Port;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

/**
 * Class that handles the EV3Sensors. Provides simplified methods to interface with the Sensors.
 * 
 * Currently handles all 4 sensors.
 * 
 * @author Patric Plattner 
 */
public class SensorArray
{
    /** Sensor sample rate in milliseconds. */
    private final int sampleRate_ = 2;

    /** Gyro sensor. */
    private EV3GyroSensor gyro_;

    /** Touch sensor. */
    private EV3TouchSensor touch_;

    /** Ultrasonic sensor */
    private EV3UltrasonicSensor ultrasonic_;

    /** Color sensor */
    private EV3ColorSensor color_;

    /**
     * Constructor for Sensor Arary.
     * 
     * @param gyroPort Port for gyro sensor.
     * @param touchPort Port for touch sensor.
     * @param ultrasonicPort Port for ultrasonic sensor.
     * @param colorPort Port for color sensor.
     */
    public SensorArray(Port gyroPort, Port touchPort, Port ultrasonicPort, Port colorPort)
    {
        this.gyro_ = new EV3GyroSensor(gyroPort);
        this.touch_ = new EV3TouchSensor(touchPort);
        this.ultrasonic_ = new EV3UltrasonicSensor(ultrasonicPort);
        this.color_ = new EV3ColorSensor(colorPort);
    }

    /**
     * Returns current rotation.
     * @return Returns current rotation.
     */
    int getRotation()
    {
        final SampleProvider sp = this.gyro_.getAngleAndRateMode();

        float [] firstSample = new float[sp.sampleSize()];
        sp.fetchSample(firstSample, 0);
        return (int) firstSample[0];
    }

    /**
     * Waits for Robot to rotate x degrees counterclockwise.
     * @param deg Degrees to rotate.
     * @throws IllegalArgumentException If deg is smaller than 1.
     */
    public void waitForCounterClockwiseRotation(int deg, int ref)
    {
        if (deg <= 0)
        {
            throw new IllegalArgumentException("Angle=" + deg + " should be > 0.");
        }

        final SampleProvider sp = this.gyro_.getAngleAndRateMode();

        float [] firstSample = new float[sp.sampleSize()];
        sp.fetchSample(firstSample, 0);
        int refValue = (int) firstSample[0];

        System.out.println("Started CounterClockwise rotation by " + deg + " degrees.");

        while(true)
        {
            float [] sample = new float[sp.sampleSize()];
            sp.fetchSample(sample, 0);
            int value = (int)sample[0];

            System.out.println("Gyro angle/rate: " + (value - refValue) + ">=" + deg);

            if(value - refValue >= deg)
            {
                break;
            }

            //Delay.msDelay(this.sampleRate_);
        }
        System.out.println("Ended rotation by " + deg + " degrees.");
    }

    /**
     * Waits for Robot to rotate x degrees clockwise.
     * @param deg Degrees to rotate.
     * @throws IllegalArgumentException If deg is smaller than 1.
     */
    public void waitForClockwiseRotation(int deg, int ref)
    {
        if (deg <= 0)
        {
            throw new IllegalArgumentException("Angle=" + deg + " should be > 0.");
        }

        final SampleProvider sp = this.gyro_.getAngleAndRateMode();

        float [] firstSample = new float[sp.sampleSize()];
        sp.fetchSample(firstSample, 0);
        int refValue = (int) firstSample[0];

        System.out.println("Started Clockwise rotation by " + deg + " degrees.");

        while(true)
        {
            float [] sample = new float[sp.sampleSize()];
            sp.fetchSample(sample, 0);
            int value = (int)sample[0];

            System.out.println("Gyro angle/rate: " + (value - refValue));

            if(value - refValue <= -1 * deg)
            {
                break;
            }

            //Delay.msDelay(this.sampleRate_);
        }
        System.out.println("Ended  rotation by " + deg + " degrees.");
    }

    /**
     * Returns true if button is pressed.
     * @return true if button is pressed and false if it isnt.
     */
    public boolean isButtonPressed()
    {
        return this.touch_.isPressed();
    }

    /**
     * Waits for button being pressed.
     */
    public void waitForButtonPressed()
    {
        while (!this.isButtonPressed())
        {
            Delay.msDelay(this.sampleRate_);
        }
    }

    /**
     * Waits for button being released.
     */
    public void waitForButtonReleased()
    {
        while (this.isButtonPressed())
        {
            Delay.msDelay(this.sampleRate_);
        }
    }

    /**
     * Returns current distance from ultrasonic sensor.
     * @return Distance in centimeters.
     */
    public int getDistance()
    {
        final SampleProvider sp = this.ultrasonic_.getDistanceMode();
        float [] sample = new float[sp.sampleSize()];
        sp.fetchSample(sample, 0);
        return (int)sample[0];
    }

    /**
     * Wait for distance to be lower or equal than x.
     * @param dist Distance in centimeters.
     * @throws IllegalArgumentException If dist is out of range [1,250].
     */
    public void waitForDistLowerEqualThan(int dist)
    {
        if (dist > 250 && dist < 1)
        {
            throw new java.lang.IllegalArgumentException("Dist=" + dist + " is out of range of [1,250].");
        }
        final SampleProvider sp = this.ultrasonic_.getDistanceMode();
        int distanceValue = 0;
        
        while (true)
        {
        	float [] sample = new float[sp.sampleSize()];
            sp.fetchSample(sample, 0);
            distanceValue = (int)sample[0];

            if (distanceValue <= dist)
            {
                break;
            }

            Delay.msDelay(this.sampleRate_);
        }
    }

    /**
     * Wait for distance to be higher or equal than
     * @param dist Distance in centimeters.
     * @throws IllegalArgumentException If dist is out of range [1,250].
     */
    public void waitForDistGreaterEqualThan(int dist)
    {
        if (dist > 250 && dist < 1)
        {
            throw new java.lang.IllegalArgumentException("Dist=" + dist + " is out of range of [1,250].");
        }
        final SampleProvider sp = this.ultrasonic_.getDistanceMode();
        int distanceValue = 0;
        
        while (true)
        {
        	float [] sample = new float[sp.sampleSize()];
            sp.fetchSample(sample, 0);
            distanceValue = (int)sample[0];

            if (distanceValue >= dist)
            {
                break;
            }
            Delay.msDelay(this.sampleRate_);
        }
    }

    /**
     * Returns reflection intensity.
     * @return Reflection intensity.
     */
    public int getReflectionIntensity()
    {
		System.out.println("Switching to Color ID Mode");
		final SampleProvider sp = this.color_.getColorIDMode();

		int sampleSize = sp.sampleSize();
		float[] sample = new float[sampleSize];

        sp.fetchSample(sample, 0);
        return (int)sample[0];
    }

    /**
     * Waits for reflection intensity to be greater than x.
     * @param intensity Reflection intensity.
     */
    public void waitForReflectionIntensityGreaterThan(int intensity)
    {
        final SampleProvider sp = this.color_.getAmbientMode();
        int intensityValue = 0;
        
        while (true)
        {
        	float [] sample = new float[sp.sampleSize()];
            sp.fetchSample(sample, 0);
            intensityValue = (int)sample[0];

            if (intensityValue > intensity)
            {
                break;
            }
            Delay.msDelay(this.sampleRate_);
        }
    }

    /**
     * Waits for reflection intensity tp be lower than x.
     * @param intensity Reflection intensity.
     */
    public void waitForReflectionIntensityLowerThan(int intensity)
    {
        final SampleProvider sp = this.color_.getAmbientMode();
        int intensityValue = 0;
        
        while (true)
        {
        	float [] sample = new float[sp.sampleSize()];
            sp.fetchSample(sample, 0);
            intensityValue = (int)sample[0];

            if (intensityValue < intensity)
            {
                break;
            }
            Delay.msDelay(this.sampleRate_);
        }
    }
}
