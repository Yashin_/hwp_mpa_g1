package project.lib;

import java.util.Random;

import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;

import lejos.hardware.port.Port;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;

/**
 * Class for controlling the 2 Motors attached to the wheels.
 * 
 * Currently capable of :
 *   * Driving forwards and backwards at speed X for time Y.
 *   * Turning by X degrees.
 *   * Dancing \[T]/.
 * 
 * @author Patric Plattner
 */
public class DrivingAssembly
{
    /** SensorArray for gyro sensor. */
    private SensorArray sArr_;

    /** Left motor of DrivingAssembly. */
    private EV3LargeRegulatedMotor leftMotor_;

    /** Right motor of DrivingAssembly. */
    private EV3LargeRegulatedMotor rightMotor_;

    /** Driving speed */
    private int speed_;


    /**
     * Standard constructor for DrivingAssembly.
     * @param leftPort Left motor Port.
     * @param rightPort Right motor Port.
     * @param sArr SensorArray for gyro sensor.
     */
    public DrivingAssembly(Port leftPort, Port rightPort, SensorArray sArr)
    {
        this.leftMotor_ = new EV3LargeRegulatedMotor(leftPort);
        this.rightMotor_ = new EV3LargeRegulatedMotor(rightPort);
        this.speed_ = 0;
        this.leftMotor_.setSpeed(this.speed_);
        this.rightMotor_.setSpeed(this.speed_);
        this.sArr_ = sArr;
      //  this.leftMotor_.synchronizeWith(new RegulatedMotor[] {this.rightMotor_});
    }

    /**
     * Sets speed.
     * @param speed Speed.
     */
    public void setSpeed(int speed)
    {
        this.speed_ = speed;
    }

    /**
     * Get speed.
     * @return Speed.
     */
    public int getSpeed()
    {
        return this.speed_;
    }

    /**
     * Drive forward for x milliseconds.
     * @param time Driving duration in miliseconds.
     */
    public void driveForward(int time)
    {
        this.leftMotor_.setSpeed(this.speed_);
        this.rightMotor_.setSpeed(this.speed_);
        this.leftMotor_.forward();
        this.rightMotor_.forward();
        Delay.msDelay(time);
        this.leftMotor_.stop();
        this.rightMotor_.stop();
        this.leftMotor_.setSpeed(0);
        this.rightMotor_.setSpeed(0);
    }

    /**
     * Drive backward for x milliseconds.
     * @param time Driving duration in miliseconds.
     */
    public void driveBackward(int time)
    {
        this.leftMotor_.setSpeed(this.speed_);
        this.rightMotor_.setSpeed(this.speed_);
        this.leftMotor_.backward();
        this.rightMotor_.backward();
        Delay.msDelay(time);
        this.leftMotor_.stop();
        this.rightMotor_.stop();
        this.leftMotor_.setSpeed(0);
        this.rightMotor_.setSpeed(0);
    }

    /**
     * Turns the wheels an given number of degree
     * @param rots Number of Degrees to turn the wheels
     */
    public void driveDegree(int degree)
    {
      //  this.leftMotor_.startSynchronization();
        this.leftMotor_.setSpeed(this.speed_);
        this.rightMotor_.setSpeed(this.speed_);
        this.leftMotor_.rotate(degree);
        this.rightMotor_.rotate(degree);
       // this.leftMotor_.endSynchronization();

        this.leftMotor_.waitComplete();
        this.rightMotor_.waitComplete();
        
        this.leftMotor_.setSpeed(0);
        this.rightMotor_.setSpeed(0);
    }

    /**
     * Turns robot by X degrees counterclockwise.
     * @param deg Amount of degrees to turn.
     */
    public void turnLeft(int deg)
    {
        this.leftMotor_.setSpeed(50);
        this.rightMotor_.setSpeed(50);
        int refValue = this.sArr_.getRotation();
        this.leftMotor_.backward();
        this.rightMotor_.forward();
        this.sArr_.waitForCounterClockwiseRotation(deg, refValue);
        this.leftMotor_.stop();
        this.rightMotor_.stop();
        this.leftMotor_.setSpeed(0);
        this.rightMotor_.setSpeed(0);
    }

    /**
     * Turns robot by X degrees clockwise.
     * @param deg Amount of degrees to turn.
     */
    public void turnRight(int deg)
    {
        this.leftMotor_.setSpeed(50);
        this.rightMotor_.setSpeed(50);
        int refValue = this.sArr_.getRotation();
        this.leftMotor_.forward();
        this.rightMotor_.backward();
        this.sArr_.waitForClockwiseRotation(deg, refValue);
        this.leftMotor_.stop();
        this.rightMotor_.stop();
        this.leftMotor_.setSpeed(0);
        this.rightMotor_.setSpeed(0);
    }

    /**
     * Makes the robot dance. \[T]/
     */
    public void dance()
    {
        Random rand = new Random();
        int count = rand.nextInt();
        for (int i = 0; i < count; ++i)
        {
            if (rand.nextInt() % 2 == 1)
            {
                this.turnLeft(rand.nextInt() % 360);
        
            }
            else
            {
                this.turnRight(rand.nextInt() % 360);
            }
        }
    }
}