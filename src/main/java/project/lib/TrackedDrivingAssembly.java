package project.lib;

import lejos.hardware.port.Port;

public class TrackedDrivingAssembly extends DrivingAssembly
{
    /**
     * Absolute X coordinate of the robot
     */
    private int posX_;

     /**
     * Absolute Y coordinate of the robot
     */
    private int posY_;

    /**
     * Current angle of the robot in degree
     */
    private int angle_;

    /**
     * Diameter of the drivingwheels in mm
     */
    private final int wheelDiameter_;

    private final double movingDistancePerDegree_;

    /**
     * Object to control the Pen
     */
    private final PenAssembly pAssembly_;

    public TrackedDrivingAssembly(int wheelDiameter, PenAssembly pAssembly, Port leftPort, Port rightPort, SensorArray sArr)
    {
        super(leftPort, rightPort, sArr);
        this.posX_ = 0;
        this.posY_ = 0;
        this.pAssembly_ = pAssembly;
        this.angle_ = 90;
        this.wheelDiameter_ = wheelDiameter;
        this.movingDistancePerDegree_ = (this.wheelDiameter_ * Math.PI) / 360.0;
    }

    /**
     * Puts the pen on the paper and starts drawing
     */
    public void draw()
    {
        pAssembly_.draw();

    }

    /**
     * Lifts the pen up and stops drawing
     */
    public void stopDrawing()
    {
        pAssembly_.stopDrawing();
    }

    /**
     * Turns the robot to an given angle
     *      90
     *       |
     *  180 ---- 0
     *       |
     *      270
     * @param angle Angle to turn to
     */
    public void turnTo(int angle)
    {
        while (angle < 0)
        {
            angle += 360;
        }
        angle %= 360;

        int correction = Math.abs(this.angle_ - angle);
        System.out.println("Turning from " + this.angle_ + " to " + angle + " by " + correction + " degrees.");
        
        if(correction == 0)
        {
            return;
        }
        else if((this.angle_ + 180 < 360) ? (angle >= this.angle_ && angle < this.angle_ + 180) : (angle >= this.angle_ && angle < 360 || angle >= 0 && angle < (this.angle_ + 180) % 360))
        {
            System.out.println("Turning left " + Math.abs(correction) + " degrees.");
            super.turnLeft(Math.abs(correction));
        }
        else
        {
            System.out.println("Turning right " + Math.abs(correction) + " degrees.");
            super.turnRight(Math.abs(correction) % 181);
        }

        this.angle_ = angle;
    }

    /**
     * Drives and turns the robot to an given position
     * @param x X-coordinate to drive to
     * @param y Y-coordinate to drive to
     */
    public void driveTo(int x, int y)
    {
        int deltaX = x - this.posX_;
        int deltaY = y - this.posY_;

        int angle = (int) Math.toDegrees(Math.tanh((double)deltaY / (double)deltaX));
        this.turnTo(angle);

        int distanceToDrive = (int)Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
        this.drive(distanceToDrive);
        this.angle_= angle;
        this.posX_ = x;
        this.posY_ = y;
    }

    /**
     * Drives the robot given millimeters in the current direction
     * @param distance Distance to drive in mm
     */
    public void drive(int distance)
    {
        int toRotateNum = (int) (distance / this.movingDistancePerDegree_);
        
        super.driveDegree(toRotateNum);

        this.posX_ += Math.cos(Math.toRadians(this.angle_)) * distance;
        this.posY_ += Math.sin(Math.toRadians(this.angle_)) * distance;
    }

    /**
     * Drives the robot in a circle.
     * Positive degree is counterclockwise
     * @param degree Degree of the circle to drive(90° means quarter circle in clockwise direction)
     * @param radius Radius of the circle to drive
     */
    public void driveCircle(int degree, int radius)
    {
        //super.driveCircle();
        this.angle_ += degree;
        this.posX_ = (int) ((this.posX_ + radius) - Math.cos(Math.toRadians(degree)) * radius);
        this.posY_ = (int) (this.posY_ + Math.sin(Math.toRadians(-1 * degree)) * radius);
    }

    
}
