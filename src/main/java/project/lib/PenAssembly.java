package project.lib;

import ev3dev.actuators.lego.motors.EV3MediumRegulatedMotor;
import ev3dev.sensors.ev3.EV3TouchSensor;

import lejos.hardware.port.Port;
import lejos.utility.Delay;


public class PenAssembly
{
    /** Motor that moves the Pen. */
    private final EV3MediumRegulatedMotor penMotor_;

    /** SensorArray for button used to calibrate position of pen. */
    private SensorArray sArr_;

    /** Speed of Pen Motor */
    private int penSpeed_;

    /** Determines whether the Pen is in drawing position. */
    private boolean drawing_;

    public PenAssembly(Port penPort, SensorArray sArr)
    {
        this.penMotor_ = new EV3MediumRegulatedMotor(penPort);
        this.sArr_ = sArr;
        this.penSpeed_ = 200;
        this.drawing_ = false;
    }

    /**
     * Elevates Pen to up position and sets position variable.
     */
    public void calibrate()
    {
        this.penMotor_.setSpeed(this.penSpeed_);
        this.penMotor_.forward();
        this.sArr_.waitForButtonPressed();
        Delay.msDelay(100);
        this.penMotor_.stop();
        this.penMotor_.setSpeed(0);
        this.drawing_ = false;
    }
    
    /**
     * Returns true if pen is lowered to drawing position, false if it isn't.
     * @return true if pen lowered, false if not.
     */
    public boolean getDrawingState()
    {
        return this.drawing_;
    }

    /**
     * Lowers pen to drawing position.
     */
    public void draw()
    {
        if (!this.drawing_)
        {
            this.penMotor_.setSpeed(this.penSpeed_);
            this.penMotor_.backward();
            Delay.msDelay(400);
            this.penMotor_.stop();
            this.penMotor_.setSpeed(0);
            this.drawing_ = true;
        }
    }

    /**
     * Lifts pen to standby position.
     */
    public void stopDrawing()
    {
        if (this.drawing_)
        {
            this.calibrate();
        }
    }
}
