package project;

import project.lib.*;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.utility.Delay;;

class Main
{
    public static void main(String[] args) {
        System.out.println("STARTING");
        SensorArray sArr = new SensorArray(SensorPort.S1, SensorPort.S2, SensorPort.S3, SensorPort.S4);
        //DrivingAssembly assembly = new DrivingAssembly(MotorPort.B, MotorPort.C, sArr);
        PenAssembly pAssembly = new PenAssembly(MotorPort.A, sArr);
        TrackedDrivingAssembly robot = new TrackedDrivingAssembly(55, pAssembly, MotorPort.B, MotorPort.C, sArr);

        /*robot.setSpeed(300);
        robot.draw();
        robot.drive(100);
        Delay.msDelay(100);
        robot.turnTo(45);
        Delay.msDelay(100);
        robot.drive(100);
        Delay.msDelay(100);
        robot.turnTo(315);
        Delay.msDelay(100);
        robot.drive(100);
        Delay.msDelay(100);
        robot.driveTo(0,100);
        Delay.msDelay(100);
        robot.driveTo(141,0);
        Delay.msDelay(100);
        robot.turnTo(90);
        Delay.msDelay(100);
        robot.drive(100);
        Delay.msDelay(100);
        robot.driveTo(0,0);
        Delay.msDelay(100);
        robot.stopDrawing();*/
        System.out.println("TURN0");
        robot.turnTo(0);
        Delay.msDelay(2000);
        System.out.println("TURN45");
        robot.turnTo(45);
        Delay.msDelay(2000);
        System.out.println("TURN115");
        robot.turnTo(115);
       
       
        System.out.println("CONNECTED");
        System.out.println("DONE");
    }
}
