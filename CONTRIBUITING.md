## Coding style and conventions

Just try to adhere loosely to the coding style already present in our source files. Here are some of the most important rules:

* Indents are soft tabs (4 spaces)
* Place your braces on the next line. Example:
```
int method(int id)
{
    /* code */
}
```
* Class member variables have to end with an underscore (example: `this.variable_`) whereas class methods shouldn't
* Even one line statements get braces (not inline).

These are just some examples. As long as you adhere loosely to what's already there there shouldn't be any problems.

Here are naming Conventions:


* Class/Interface names in `PascalCase`
* Class member variables in `camelCase_` (don't forget the underscore)
* Class methods in `camelCase` (no underscore)
* Local Variables in `camelCase`

Do not make any member variables public. Use getters and setters wherever needed.

While this doesn't necessarilly belong here, it would be silly to devote a whole section for it, but we want everyone to document your code. We use Doxygen with the Javadoc-style comments. Again: just loosely adhere to the documentation style that is already in use in the project and you will be fine. Here some important points.

* Everything you touch should have the `@author Your Name` tag in it
* Please don't push commits that have undocumented code.
* NEVER do a pull request with undocumented code.

* All Javadoc comments should be stretched across multiple lines even if not necessary (for consistency)
```
/**
 * This is a function or method. Even though this fits in one line we use this multi line aproach.
 */
 void setMember1(int val);
```
* Setters and getters are to be documented as well. Just document what variable you are setting and getting and make empty `@param` and/or `@return` tags.

## Our workflow with git

We use a Master-Development-Feature Branch workflow. For those of you who don't know what that means, here is a quick rundown:

* The Master branch will not see any individual commits directly to it (unless we change something like the LICENSE or the README.md, so basically no code is commited to the master branch directly)
* The Development branch (ours is called `dev`) is where in progress development is merged onto. And yes, I mean *merged onto* and not commited, but I will go into detail on that shortly. Whenever we feel, that the current state of the development branch is good enough for another release we create a pull request to merge `dev` onto `master`.
* Feature branches are where actual development takes place. For every new feature a new branch is created (take note that we do not define the term "feature" strictly, just use whatever definition of that term that you want). You then commit and push onto that branch during development. Once you are done with your feature you create a pull request to merge onto `dev`. Then the code is reviewed and if everything is alright it will be merged onto the `dev` branch. Please keep pull requests between feature branches to a minimum.
* You are also encouraged to look at other people's code and improve it. If the feature has been finished and merged onto the dev branch already, the way we want you to do it is to create a new feature branch that follows this naming scheme: `feature_or_function/method-patch` wherein you specify what you are changing. Once you are done create a pull request to the `dev` branch. If the feature is still in active development on its branch do the same thing but create the pull request to the feature branch. This is to ensure that we can look at both pieces of code and decide what is better instead of having to go between commits.
> Please choose the lowest level of describing what you are changing. If you are changing only one class method, call the branch `method_class-patch`. If you are changing multiple methods, create multiple branches unless the changes are related to each other. If you change multiple methods just call it `class-patch`. If you are changing free functions, then take the name of the lowest level feature and call it `feature-patch`.

I think that this is going without saying, but your commits should have descriptive commit messages. Please think twice before commiting, since changing this after the fact can be a pain.